package com.digibelt.demo;

public enum CustomerStatus {
    ImportedLead, NotContacted, Contacted, Customer, ClosedLost
}